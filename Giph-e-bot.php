<?php
/*
 * Giph-e-bot
 *
 * A Telegram bot for searching Giphy
 *
 * NB this is built atop code from BryceBot's telegram_ipc handler.
 * https://github.com/Giphy/GiphyAPI
 *
 * Author: Bryce Chidester <bryce@cobryce.com>
 * License: BSD 2-clause
 */

/* 
help - Print a help message. It's not very helpful. Don't waste your time.
getgif - Retrieve a random gif, optionally based on a search phrase.
random - Same as /getgif
randomgif - Same as /getgif
sticker - Retrieve a random sticker gif, optionally based on a search phrase.
randomsticker - Same as /sticker
getsticker - Same as /sticker
*/

require_once('private.inc.php');	// Contains API keys and tokens
require_once('library.inc.php');

define('DEBUG_BOT', true);
define('LOG_FILE', '/tmp/Giph-e-bot.log');
define('TELEGRAM_TOKEN', GIPHY_TG_TOKEN);	// Set the token for our API calls.
define('BOT_USER_AGENT', 'Giph-e-bot/1.0');
ini_set('user_agent', BOT_USER_AGENT);

// Set an absolute time limit so that any blocking function does not cause too many pages to back up on the server
set_time_limit(180);

// Logging comes first. Logic at bottom.
$raw_input = log_request(LOG_FILE);

if(isset($_REQUEST['setwebhook']))
{
	// Verify our API token
	// {"ok":true,"result":{"id":119881304,"first_name":"BryceBot","username":"BryceBot"}}
	$me = telegram_api("getMe");
	if($me === FALSE ||
	   $me->ok != "true")
	{
		codelog("There was some sort of error verifying our API key.", $me);
		return false;
	}
	
	// TODO: Update the BOT_USERNAME in the shared memory.
	
	$my_url = sprintf('http%s://%s%s', 
	                  (isset($_SERVER['HTTPS']) ? 's' : ''),
	                  $_SERVER['HTTP_HOST'],
	                  $_SERVER['PHP_SELF']);
	$sethook = telegram_api("setWebhook", array('url' => $my_url));
	if($sethook === FALSE ||
	   $sethook->ok != "true")
	{
		header("HTTP/1.1 400 Webhook not set.");
		codelog("There was some sort of error while setting the webhook URL.", $me);
		return false;
	}
	die("Okay, web hook URL set.\n");

} elseif(isset($_REQUEST['q']))
{
	$search = giphy_random($_REQUEST['q']);
	var_dump($search);
	die();

} elseif(isset($_REQUEST['vd']))
{
	var_dump($_SERVER);
	die();
}

$msg = json_decode($raw_input);
if(!$msg)	// JSON error
{
	header("HTTP/1.1 400 Message not sent.");
	debuglog("Raw Input: ", $raw_input);
	debuglog("JSON decode: ", $json);
	die();
}

/* Example message:
{  
   "update_id":827336331,
   "message":{  
	  "message_id":7,
	  "from":{  
		 "id":88415510,
		 "first_name":"Bryce",
		 "last_name":"Chidester",
		 "username":"brycec"
	  },
	  "chat":{  
		 "id":88415510,
		 "first_name":"Bryce",
		 "last_name":"Chidester",
		 "username":"brycec"
	  },
	  "date":1435619779,
	  "text":"Meow"
   }
}
{"update_id":827336331,"message":{"message_id":7,"from":{"id":88415510,"first_name":"Bryce","last_name":"Chidester","username":"brycec"},"chat":{"id":88415510,"first_name":"Bryce","last_name":"Chidester","username":"brycec"},"date":1435619779,"text":"Meow"}}

Inline:
{"update_id":928191097,"inline_query":{"id":"379741724398197061","from":{"id":88415510,"first_name":"Bryce","last_name":"Chidester","username":"brycec"},"query":"Meow","offset":""}}
*/

// Verify we're handling updates in order
// TODO what if the SHM is out of order? Perhaps a threshold? Or
//      or just a dumb list of all ID's already handled?

$key = ftok($_SERVER['SCRIPT_FILENAME'], "A");
debuglog(sprintf("Shm/Semaphore key: (%s) %d 0x%X", $_SERVER['SCRIPT_FILENAME'], $key, $key));
list($shm, $sem) = open_shared_memory($key);
if(!$shm ||
   !$sem)
{
	header("HTTP/1.1 400 Message not sent.");
	codelog("There was an error obtaining the semaphore or attaching to shared memory.");
	die();
}
load_persistent_data($shm);
check_message_sequence($shm, $msg);	// Can die()
close_shared_memory($shm, $sem);


if(isset($msg->inline_query->query))
{
	debuglog("InlineQuery: ", $msg->inline_query->query);
	return;	// XXX Because this doesn't work....
	
	// Limit of '50' imposed by Telegram InlineBots API.
	$res = giphy_api("search", "limit=50&q=".urlencode($msg->inline_query->query));
	if(!$res->data)
	{
		// Return an empty set
		debuglog("giphy_api returned an empty set:", $res);
		telegram_api("answerInlineQuery", array(
		    'inline_query_id' => $msg->inline_query->id, 
		    'results' => array()));
		return;
	}
	debuglog("giphy_api returned:", $res);
	
	// $res->data is an array of results
	// $res->data[]->original->url is the InlineQueryResultGif, ->mp4 is the InlineQueryResultMpeg4Gif, also ->width and ->height
	$InlineResults = array();
	foreach($res->data as $ea)
	{
		/* GIF
		$InlineResults[] = array(
			'type' => 'gif',
			'id' => "".$ea->id,
			'gif_url' => $ea->images->original->url,
			'gif_width' => $ea->images->original->width,
			'gif_height' => $ea->images->original->height,
			'thumb_url' => $ea->images->original_still->url,
			'caption' => $ea->caption
			);
		*/
		/* MP4
		$InlineResults[] = array(
			'type' => 'mpeg4_gif',
			'id' => $ea->id,
			'mpeg4_url' => $ea->images->original->mp4,
			'mpeg4_width' => $ea->images->original->width,
			'mpeg4_height' => $ea->images->original->height,
			'thumb_url' => $ea->images->original_still->url,
			'caption' => $ea->caption
			);
		*/
	}
	debuglog("Returning to telegram api answerInlineQuery:", $InlineResults);
	telegram_api("answerInlineQuery", array(
	    'inline_query_id' => "".$msg->inline_query->id,
	    'results' => $InlineResults));
#	    'results' => json_encode($InlineResults)));
#	    'results' => urlencode(json_encode($InlineResults))));
	
} elseif(isset($msg->message->text))
{
	list($command, $args) = parse_message_text_into_command_args($msg->message->text);
	
	debuglog("Command: ", $command);
	debuglog("Args: ", $args);
	
	
	if($command == "/help" ||
	   $command == "/start")
	{
		telegram_api("sendMessage", array(
		    'chat_id' => $msg->message->chat->id,
		    'reply_to_message_id' => $msg->message->message_id,
		    'text' => "Hi, I'm Giph-e-bot. I look up stuff on Giphy and return the results."
		    ));
	}
	
	/* Only handle text that calls our commands! */
	if($command == "/getgif" ||
	   $command == "/random" ||
	   $command == "/randomgif")
	{
		// Get info of a random giphy
		$res = giphy_random($args);
		if(!$res->data)
		{
			telegram_api("sendMessage", array(
				'chat_id' => $msg->message->chat->id,
				'reply_to_message_id' => $msg->message->message_id,
				'text' => "Giphy returned no results found for '$args'"
				));
			return;
		}
		
		telegram_api("sendChatAction", array(
		    'action' => 'upload_photo', 'chat_id' => $msg->message->chat->id));
		
		// Download the image so we can re-upload it
		$image_file = download_file($res->data->image_url, "Giph_");
		debuglog("download_file returned:", $image_file);
		if($image_file !== false)
		{
			$cf = curl_file_create($image_file['file'], $image_file['mimetype'], "{$res->data->id}.gif");
			$ret = telegram_api("sendPhoto", array(
				'chat_id' => $msg->message->chat->id,
				'reply_to_message_id' => $msg->message->message_id,
				'photo' => $cf,
				//'duration' => $res->data->image_frames/30, /* frames -> seconds, somehow*/
				'caption' => $res->data->url
				));
			unlink($image_file['file']);
		}
		
		if($image_file === false)
			telegram_api("sendMessage", array(
			    'chat_id' => $msg->message->chat->id,
			    'reply_to_message_id' => $msg->message->message_id,
			    'text' => "I'm sorry, bro, there was an error while I was fetching you a gif.\n".
			              $res->data->url
			    ));
		if($ret === false)
			telegram_api("sendMessage", array(
			    'chat_id' => $msg->message->chat->id,
			    'reply_to_message_id' => $msg->message->message_id,
			    'text' => "I'm sorry, bro, the Telegram API returned an error while I was sending the requested image.\n".
			              $res->data->url
			    ));
	}

	if($command == "/sticker" ||
	   $command == "/randomsticker" ||
	   $command == "/getsticker")
	{
		// Get info of a random giphy
		$res = giphy_stickers_random($args);
		if(!$res->data)
		{
			telegram_api("sendMessage", array(
				'chat_id' => $msg->message->chat->id,
				'reply_to_message_id' => $msg->message->message_id,
				'text' => "Giphy returned no sticker results found for '$args'"
				));
			return;
		}
		
		telegram_api("sendChatAction", array(
		    'action' => 'upload_photo', 'chat_id' => $msg->message->chat->id));
		
		// Download the image so we can re-upload it
		$image_file = download_file($res->data->image_url, "Giph_");
		debuglog("download_file returned:", $image_file);
		$ret = null;
		if($image_file !== false)
		{
			$cf = curl_file_create($image_file['file'], $image_file['mimetype'], "{$res->data->id}.gif");
			$ret = telegram_api("sendPhoto", array(
				'chat_id' => $msg->message->chat->id,
				'reply_to_message_id' => $msg->message->message_id,
				'photo' => $cf,
				// 'duration' => $res->data->image_frames/30, /* frames -> seconds, somehow*/
				'caption' => $res->data->url
				));
			unlink($image_file['file']);
		}
		
		if($image_file === false)
			telegram_api("sendMessage", array(
			    'chat_id' => $msg->message->chat->id,
			    'reply_to_message_id' => $msg->message->message_id,
			    'text' => "I'm sorry, bro, there was an error while I was fetching you a sticker.\n".
			              $res->data->url
			    ));
		if($ret === false)
			telegram_api("sendMessage", array(
			    'chat_id' => $msg->message->chat->id,
			    'reply_to_message_id' => $msg->message->message_id,
			    'text' => "I'm sorry, bro, the Telegram API returned an error while I was sending the requested sticker.\n".
			              $res->data->url
			    ));
	}
}


// Common method for API communications.
// Returns FALSE for transport/communication errors. Passes-through the JSON
// object otherwise.


function giphy_api($method, $args, $section="gifs")
{
	$url = sprintf('http://api.giphy.com/v1/%s/%s?%s&api_key=%s',
	               $section, $method, $args, GIPHY_TOKEN);
	debuglog("Request URL:", $url);
	debuglog("Request Args:", $args);
	
	$response = file_get_contents($url);
	debuglog("Giphy response[raw]:", $response);
	if(!$response)
	{
		debuglog("Giphy error!");
		return false;
	}
	
	$response_json = json_decode($response);
	debuglog("Giphy response[obj]:", $response_json);
	if(!$response_json)
	{
		codelog("Giphy JSON error!");
		return false;
	}
	
	if($response_json->meta->msg != "OK")
		codelog("Giphy Warning, API returned an error response.", $response_json);
	
	return $response_json;
}

function giphy_random($tag=null)
{
	if($tag)
		return giphy_api("random", "tag=".urlencode($tag));
	return giphy_api("random", "");
}

function giphy_stickers_random($tag)
{
	if($tag)
		return giphy_api("random", "tag=".urlencode($tag), "stickers");
	return giphy_api("random", "", "stickers");
}

// Fall-through to a 204
//header("HTTP/1.1 204 No Content");
// "Bot 2:59 PM You should response with an ok, I would suggest a 200 code answer."
// Fall-through 200/ok
?>ok

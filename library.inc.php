<?php
/*
 * Common library functions for Bryce's Telegram Bots
 *
 * Author: Bryce Chidester <bryce@cobryce.com>
 * License: BSD 2-clause
 */

define('SHM_IDX_LAST_UPDATE', 1);
define('SHM_IDX_BOT_USERNAME', 2);
define('SHM_MEMORY_SIZE', 512);

function log_request($file)
{
	$fp = fopen($file, 'a');
	$start = microtime(true);
	while(!flock($fp, LOCK_EX))
	{
		// Timeout after 2 seconds
		if( (microtime(true) - $start) > 2)
		{
			error_log("Timed out trying to acquire log file lock!!");
			die(header("HTTP/1.1 400 Message not sent."));
		}
	}
	fwrite($fp, "=====================================\n");
	fwrite($fp, "Datestamp\n");
	fwrite($fp, date('r').PHP_EOL);

	fwrite($fp, "GET\n");
	fwrite($fp, print_r($_GET, true));

	fwrite($fp, "POST\n");
	fwrite($fp, print_r($_POST, true));

	fwrite($fp, "Raw POST data\n");
	$raw_input = file_get_contents("php://input");
	fwrite($fp, $raw_input.PHP_EOL);

	fwrite($fp, "COOKIE\n");
	fwrite($fp, print_r($_COOKIE, true));

	fwrite($fp, "FILES\n");
	fwrite($fp, print_r($_FILES, true));
	foreach($_FILES as $k => $v)
	{
		fwrite($fp, "==$k -- {$v['name']}==\n");
		fwrite($fp, file_get_contents($v['tmp_name']).PHP_EOL);
	}

	fwrite($fp, "SESSION\n");
	fwrite($fp, @print_r($_SESSION, true));

	fwrite($fp, "ENV\n");
	fwrite($fp, print_r($_ENV, true));

	fwrite($fp, "GLOBALS\n");
	fwrite($fp, print_r($GLOBALS, true));

	fwrite($fp, "REQUEST\n");
	fwrite($fp, print_r($_REQUEST, true));

	fwrite($fp, "HTTP Response Header\n");
	fwrite($fp, @print_r($http_response_header, true));

	fwrite($fp, "Server\n");
	fwrite($fp, print_r($_SERVER, true));

	fflush($fp);
	flock($fp, LOCK_UN);
	fclose($fp);
	
	return $raw_input;
}

function load_persistent_data($shm)
{
	if(shm_has_var($shm, SHM_IDX_BOT_USERNAME))
	{
		define('BOT_USERNAME', shm_get_var($shm, SHM_IDX_BOT_USERNAME));
		debuglog("BOT_USERNAME retrieved from shared memory:", BOT_USERNAME);
	} else
	{
		debuglog("No BOT_USERNAME known. Fetching from the API...");
		$me = telegram_api("getMe");
		if($me === FALSE ||
		   $me->ok != "true")
		{
			codelog("Error while retreiving my own username. Falling back to a hardcoded value.", $me);
			define('BOT_USERNAME', 'Telegram_Bot');
			break;
		}
		define('BOT_USERNAME', $me->result->username);
		shm_put_var($shm, SHM_IDX_BOT_USERNAME, BOT_USERNAME);
		debuglog("BOT_USERNAME retrieved from API, stored in shared memory:", BOT_USERNAME);
	}
}

function check_message_sequence($shm, $msg)
{
	/* Disabled, as there's no clear-cut case on how to handle this.
	$last_update_id = 0;
	if(shm_has_var($shm, SHM_IDX_LAST_UPDATE))
	{
		$last_update_id = shm_get_var($shm, SHM_IDX_LAST_UPDATE);
		debuglog("last_update_id retrieved from shared memory:", $last_update_id);
	}
	debuglog("Last update ID: {$last_update_id}  Message ID: {$msg->update_id}");
	if($msg->update_id <= $last_update_id)
	{
		codelog("Passed an old update_id! Skipping.".
			    "Last ID: {$last_update_id}  ".
			    "This ID: {$msg->update_id}");
		header("HTTP/1.1 400 Message already received.");
		die();
	}
	*/
	shm_put_var($shm, SHM_IDX_LAST_UPDATE, $msg->update_id);
}

function open_shared_memory($key)
{
	debuglog(sprintf("Obtaining semaphore for key: %d 0x%X", $key, $key));
	$sem = sem_get($key);
	sem_acquire($sem);	// Blocks
	debuglog("Semaphore obtained. Attaching to shared memory.");
	$shm = shm_attach($key, SHM_MEMORY_SIZE);
	return array($shm, $sem,	// For compatibility with list(), we also index numerically
	             'shm' => $shm, 'sem' => $sem);
}

function close_shared_memory($shm, $sem)
{
	debuglog("Closing shared memory and releasing semaphore.");
	shm_detach($shm);
	sem_release($sem);
}

function parse_message_text_into_command_args($text)
{
	debuglog("SpacePos: ", $space = strpos($text, " "));
	debuglog("Command >".($command = trim(substr($text, 0, $space)))."<");
	debuglog("Args >".($args = trim(substr($text, $space)))."<");
	if(!$command)
	{
		debuglog("Rectifying empty command", $command);
		$command = trim($args);
		$args = "";
	}
	
	if(strpos($command, '@') !== false)	// Command has a @, strip
	{
		$atpos = strpos($command, '@');
		debuglog("AtPos:", $atpos);
		
		$afterat = trim(substr($command, $atpos+1));	// +1 to skip the @
		$command = trim(substr($command, 0, $atpos));
		debuglog("Command >$afterat<");
		debuglog("After At >$afterat<");
		
		if($afterat != BOT_USERNAME)
		{
			codelog("Ignoring command that was directed at some other bot!");
			return false;
		}
	}
	
	// Commands are always lower-case
	$command = strtolower($command);
	
	debuglog("Command: ", $command);
	debuglog("Args: ", $args);
	
	return array($command, $args,	// For compatibility with list(), we also index numerically
	             'command' => $command, 'args' => $args);
}

// Common method for API communications.
// Returns FALSE for transport/communication errors. Passes-through the JSON
// object otherwise.
function telegram_api($method, $args=null)
{
	if(!defined('TELEGRAM_TOKEN'))	// Sanity check
		return false;
	if(!defined('BOT_USER_AGENT'))	// PHP's default for user_agent is null anyways.
		define('BOT_USER_AGENT', ini_get('user_agent'));
	
/* Replaced with cURL for easy uploading
	// Setup the stream context
	$context = stream_context_create();
	stream_context_set_params($context, array("notification" => 'stream_notifier'));
	$options = array();
	$options['http']['method'] = 'POST';
	//$options['http']['header'] = 'Content-type: application/x-www-form-urlencoded';
	$options['http']['header'] = 'Content-type: multipart/form-data';
	$options['http']['user_agent'] = BOT_USER_AGENT;
	if(gettype($args) == "array")
		$args = http_build_query($args);
	$options['http']['content'] = $args;
	$options['ssl']['verify_peer'] = true;
	$options['ssl']['peer_name'] = 'api.telegram.org';	// TODO Hard-coded, should be derived from apibaseurl
	$options['ssl']['verify_peer_name'] = true;
	stream_context_set_option($context, $options);
	
	$url = sprintf("https://api.telegram.org/bot%s/%s",
	               TELEGRAM_TOKEN, $method);
	debuglog("Request URL:", $url);
	debuglog("Request Args:", $args);
	
	$response = file_get_contents($url, false, $context);
	debuglog("{$method} response[raw]:", $response);
	if(!$response)
	{
		debuglog("{$method} error!");
		return false;
	}
*/
	
	$url = sprintf("https://api.telegram.org/bot%s/%s",
	               TELEGRAM_TOKEN, $method);
	
	debuglog("Request URL:", $url);
	debuglog("Request Args:", $args);
	
	$ch = curl_init($url);
	curl_setopt_array($ch, array(
	    CURLOPT_USERAGENT => BOT_USER_AGENT,
	    CURLOPT_AUTOREFERER => true,
	    CURLOPT_FAILONERROR => true,
	    CURLOPT_FOLLOWLOCATION => false,
	    CURLOPT_HEADER => false,
	    //CURLOPT_HEADER => true,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_PROTOCOLS => CURLPROTO_HTTP | CURLPROTO_HTTPS,
	    CURLOPT_SAFE_UPLOAD => true,
	    CURLOPT_POST => true,
	    CURLOPT_POSTFIELDS => $args,
	    CURLINFO_HEADER_OUT => true,
	    //CURLOPT_STDERR => $log_fh
	    ));
	// curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
	
	$response = curl_exec($ch);
	debuglog("{$method} response[raw]:", $response);
	debuglog("{$method} curl_getinfo:", curl_getinfo($ch));
	debuglog("{$method} curl_error:", curl_error($ch));
	curl_close($ch);
	
	if($response === false)
	{
		debuglog("{$method} error!");
		return false;
	}
	
	/* Experimental parsing of headers ourselves
	 * Not working reliably for Telegram, for whatever reason
	//list($headers, $body) = explode("\r\n\r\n", $response);
	//debuglog("Headers:", $headers);
	//debuglog("Body:", $body);
	//$response_json = json_decode($body);
	*/
	$response_json = json_decode($response);
	debuglog("{$method} response[obj]:", $response_json);
	if(!$response_json)
	{
		codelog("{$method} JSON error!");
		return false;
	}
	
	if($response_json->ok != "true")
		codelog("{$method} Warning, API returned an error response.", $response_json);
	
	return $response_json;
}

function stream_notifier($notification_code, $severity, $message, 
    $message_code, $bytes_transferred, $bytes_max)
{
	debuglog("stream_notifier(".
	         "notification_code={$notification_code}, ".
	         "severity={$severity}, ".
	         "message={$message}, ".
	         "message_code={$message_code}, ".
	         "bytes_transferred={$bytes_transferred}, ".
	         "bytes_max={$bytes_max}".
	         ")");
}

function download_file($url, $prefix)
{
	ini_set('track_errors', true);
	
	$temp_file_name = tempnam(sys_get_temp_dir(), $prefix);
	debuglog("Temp file: $temp_file_name");
	
	$tmp_fh = fopen($temp_file_name, 'a+');
	if($tmp_fh === false)
	{
		codelog("There was an error while opening the temporary file:", $php_errormsg);
		return false;
	}
	$url_fh = fopen($url, 'r');
	if($url_fh === false)
	{
		codelog("There was an error while opening the URL:", $php_errormsg);
		fclose($tmp_fh);
		return false;
	}
	while(!feof($url_fh))
		fwrite($tmp_fh, fread($url_fh, 8192));
	fclose($url_fh);
	fclose($tmp_fh);
	
	$finfo_fh = finfo_open();
	$mimetype = finfo_file($finfo_fh, $temp_file_name, FILEINFO_MIME_TYPE);
	debuglog("mimetype: ", $mimetype);
	
	finfo_close($finfo_fh);
	
	$imagetype = exif_imagetype($temp_file_name);
	debuglog("exif_imagetype:", $imagetype);
	
	$fileext = image_type_to_extension($imagetype);
	debuglog("imagetype_to_extension:", $fileext);
	
	// Come up with a clean, sanitized version of the filename,
	// and something about the extension
	$urlpath = parse_url($url, PHP_URL_PATH);
	debuglog("URL Path (for filename):", $urlpath);
	
	//$basename = pathinfo($filename, PATHINFO_BASENAME);
	$basename = basename($urlpath, $fileext);
	debuglog("Basename({$fileext}):", $basename);
	
	// Build a clean filename with the basename and appropriate file extension,
	// required for the Telegram API specifically which checks only based on extension.
	// NB fileext includes the period.
	$filename = sprintf("%s%s", $basename, $fileext);
	debuglog("Rebuilt filename:", $filename);
	
	return array('file' => $temp_file_name, 'mimetype' => $mimetype,
	             'imagetype' => $imagetype, 'fileext' => $fileext,
	             'basename' => $basename, 'filename' => $filename);
}

function debuglog($data, $var=null)
{
	if(!defined('DEBUG_BOT'))	// Sanity check
		define('DEBUG_BOT', false);
	
	if(DEBUG_BOT)	// This should be the only instance of this [that's necessary]
	{
		codelog($data, $var);
	}
}

function codelog($data, $var=null)
{
	if(!defined('LOG_FILE'))	// Sanity check
		return;
	
	$data = addcslashes($data, "\0..\37\177..\377");	// Escape the hidden characters (octal)
	//$l = date("[H:i:s] ").basename(__FILE__).":".__LINE__.": ".trim($data);
	//$l = date("[H:i:s] ").basename(__FILE__).": ".trim($data);
	$l = sprintf("[%s] %s", date('H:i:s'), trim($data));
	
	if($var)
	{
		if($var instanceof Exception) // Carefully output exceptions, or we recurse infinitely
		{
			$l .= sprintf(" Excpetion: %s\nCode: %s\nLocation: %s:%d\nTraceback: %s",
			              print_r($var->getMessage(), true),
			              print_r($var->getCode(), true),
			              $var->getFile(),
			              $var->getLine(),
			              print_r($var->getTraceAsString(), true));
		} else
		{
			$l .= " " . print_r($var, true);
		}
	}
	error_log(trim($l).PHP_EOL, 3, LOG_FILE);
}


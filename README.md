Hello and welcome to the wonderful world of... nothing. There's very little wonderful in this world.
I'm not really sure where I was going with this, but it got dark. Fast.

Telegram Bots
##############

I've written a Telegram bot or two. This is the source code repository. That's all there is to it. Don't expect frequent updates, or extensive documentation.

Telegram Bot API Documentation: https://core.telegram.org/bots/api

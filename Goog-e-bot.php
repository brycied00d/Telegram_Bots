<?php
/*
 * Goog-e-bot
 *
 * A Telegram bot for searching Google
 *
 * https://developers.google.com/custom-search/json-api/v1/using_rest
 *
 * Author: Bryce Chidester <bryce@cobryce.com>
 * License: BSD 2-clause
 */

/* 
help - Print a help message. It's not very helpful. Don't waste your time.
google - Do a Google search
image - Perform a Google Image search
get - Similar to /image, returns just the image and its URL without information about the search results.

TODO, if possible:
news?
videos?
*/

require_once('private.inc.php');	// Contains API keys and tokens
require_once('library.inc.php');

define('DEBUG_BOT', true);
define('LOG_FILE', '/tmp/Goog-e-bot.log');
define('GOOGY_SEARCH_RESULTS', 1);
define('TELEGRAM_TOKEN', GOOGY_TG_TOKEN);	// Set the token for our API calls.
define('BOT_USER_AGENT', 'Goog-e-bot/1.0');
ini_set('user_agent', BOT_USER_AGENT);

// Set an absolute time limit so that any blocking function does not cause too many pages to back up on the server
set_time_limit(180);

// Logging comes first. Logic at bottom.
$raw_input = log_request(LOG_FILE);

if(isset($_REQUEST['setwebhook']))
{
	// Verify our API token
	// {"ok":true,"result":{"id":119881304,"first_name":"BryceBot","username":"BryceBot"}}
	$me = telegram_api("getMe");
	if($me === FALSE ||
	   $me->ok != "true")
	{
		codelog("There was some sort of error verifying our API key.", $me);
		return false;
	}
	
	// TODO: Update the BOT_USERNAME in the shared memory.
	
	$my_url = sprintf('http%s://%s%s', 
	                  (isset($_SERVER['HTTPS']) ? 's' : ''),
	                  $_SERVER['HTTP_HOST'],
	                  $_SERVER['PHP_SELF']);
	$sethook = telegram_api("setWebhook", array('url' => $my_url));
	if($sethook === FALSE ||
	   $sethook->ok != "true")
	{
		header("HTTP/1.1 400 Webhook not set.");
		codelog("There was some sort of error while setting the webhook URL.", $me);
		return false;
	}
	die("Okay, web hook URL set.\n");

} elseif(isset($_REQUEST['q']))
{
	$search = google_search($_REQUEST['q']);
	var_dump($search);
	die();

} elseif(isset($_REQUEST['vd']))
{
	var_dump($_SERVER);
	die();
}

$msg = json_decode($raw_input);
if(!$msg)	// JSON error
{
	header("HTTP/1.1 400 Message not sent.");
	debuglog("Raw Input: ", $raw_input);
	debuglog("JSON decode: ", $json);
	die();
}

/* Example message:
{  
   "update_id":827336331,
   "message":{  
	  "message_id":7,
	  "from":{  
		 "id":88415510,
		 "first_name":"Bryce",
		 "last_name":"Chidester",
		 "username":"brycec"
	  },
	  "chat":{  
		 "id":88415510,
		 "first_name":"Bryce",
		 "last_name":"Chidester",
		 "username":"brycec"
	  },
	  "date":1435619779,
	  "text":"Meow"
   }
}
{"update_id":827336331,"message":{"message_id":7,"from":{"id":88415510,"first_name":"Bryce","last_name":"Chidester","username":"brycec"},"chat":{"id":88415510,"first_name":"Bryce","last_name":"Chidester","username":"brycec"},"date":1435619779,"text":"Meow"}}
*/

// Verify we're handling updates in order
// TODO what if the SHM is out of order? Perhaps a threshold? Or
//      or just a dumb list of all ID's already handled?

$key = ftok($_SERVER['SCRIPT_FILENAME'], "B");
debuglog(sprintf("Shm/Semaphore key: (%s) %d 0x%X", $_SERVER['SCRIPT_FILENAME'], $key, $key));
list($shm, $sem) = open_shared_memory($key);
if(!$shm ||
   !$sem)
{
	header("HTTP/1.1 400 Message not sent.");
	codelog("There was an error obtaining the semaphore or attaching to shared memory.");
	die();
}
load_persistent_data($shm);
check_message_sequence($shm, $msg);	// Can die()
close_shared_memory($shm, $sem);
	
if(isset($msg->message->text))
{
	list($command, $args) = parse_message_text_into_command_args($msg->message->text);
	
	debuglog("Command: ", $command);
	debuglog("Args: ", $args);
	
	if($command == "/help" ||
	   $command == "/start")
	{
		telegram_api("sendMessage", array(
		    'chat_id' => $msg->message->chat->id,
		    'reply_to_message_id' => $msg->message->message_id,
		    'text' => "Hi, I'm Goog-e-bot. I look up stuff on Google.\n".
		              "/google <query> - Perform a Google web serarch and return the top result.\n".
		              "/image <query> - Perform a Google Image search and send back the top image."
		    ));
	}
	
	if($command == "/google")
	{
		if(!$args)
		{
			telegram_api("sendMessage", array(
			    'chat_id' => $msg->message->chat->id,
			    'reply_to_message_id' => $msg->message->message_id,
			    'text' => "Hey there! I need a search term or terms in order to Google."
			    ));
			return;
		}
		
		telegram_api("sendChatAction", array(
		    'action' => 'typing', 'chat_id' => $msg->message->chat->id));
		
		$res = google_search($args, GOOGY_SEARCH_RESULTS);
		
		// If we got a spelling suggestino, we need to re-Google using that.
		if(isset($res->spelling))
		{
			debuglog("Google provided a spelling suggestion. Re-executing search.", $res->spelling);
			
			telegram_api("sendMessage", array(
			    'chat_id' => $msg->message->chat->id,
			    'reply_to_message_id' => $msg->message->message_id,
			    'text' => "Searching for '{$res->spelling->correctedQuery}' instead."
			    ));
			
			telegram_api("sendChatAction", array(
			    'action' => 'typing', 'chat_id' => $msg->message->chat->id));
			
			$res = google_search($args, GOOGY_SEARCH_RESULTS);
		}
		
		$r = (array)$res->queries->request;
		telegram_api("sendMessage", array(
		    'chat_id' => $msg->message->chat->id,
		    'reply_to_message_id' => $msg->message->message_id,
		    'text' => sprintf("%s total results returned for '%s', here is the top result:",
		                      $res->searchInformation->formattedTotalResults,
		                      $r[0]->searchTerms
/* When returning multiple results... PS Please fix "here's" to here is/here are
		    'text' => sprintf("%s total results returned for '%s', here's %d",
		                      $res->searchInformation->formattedTotalResults,
		                      $r[0]->searchTerms,
		                      (GOOGY_SEARCH_RESULTS < $res->searchInformation->totalResults ?
		                          GOOGY_SEARCH_RESULTS : $res->searchInformation->totalResults)
*/
		    )));
		
		if($res->searchInformation->totalResults > 0)
		{
			foreach($res->items as $i)
			{
				telegram_api("sendMessage", array(
				    'chat_id' => $msg->message->chat->id,
				    'reply_to_message_id' => $msg->message->message_id,
				    'text' => sprintf("%s (%s)\n%s",
				                      $i->title,
				                      $i->link,
				                      $i->snippet
				    )));
			}
		}
	}

	if($command == "/image")
	{
		if(!$args)
		{
			telegram_api("sendMessage", array(
			    'chat_id' => $msg->message->chat->id,
			    'reply_to_message_id' => $msg->message->message_id,
			    'text' => "Hey there! I need a search term or terms in order to Google Image search."
			    ));
			return;
		}
		
		telegram_api("sendChatAction", array(
		    'action' => 'typing', 'chat_id' => $msg->message->chat->id));
		
		$res = google_search($args, GOOGY_SEARCH_RESULTS, "image");
		
		// If we got a spelling suggestino, we need to re-Google using that.
		if(isset($res->spelling))
		{
			debuglog("Google Image provided a spelling suggestion. Re-executing search.", $res->spelling);
			
			telegram_api("sendMessage", array(
			    'chat_id' => $msg->message->chat->id,
			    'reply_to_message_id' => $msg->message->message_id,
			    'text' => "Searching for '{$res->spelling->correctedQuery}' instead."
			    ));
			
			telegram_api("sendChatAction", array(
			    'action' => 'typing', 'chat_id' => $msg->message->chat->id));
			
			$res = google_search($args, GOOGY_SEARCH_RESULTS, "image");
		}
		
		$r = (array)$res->queries->request;
		telegram_api("sendMessage", array(
		    'chat_id' => $msg->message->chat->id,
		    'reply_to_message_id' => $msg->message->message_id,
		    'text' => sprintf("%s total results returned for '%s', here is the top result:",
		                      $res->searchInformation->formattedTotalResults,
		                      $r[0]->searchTerms
/*
		    'text' => sprintf("%s total results returned for '%s', here's %d",
		                      $res->searchInformation->formattedTotalResults,
		                      $r[0]->searchTerms,
		                      (GOOGY_SEARCH_RESULTS < $res->searchInformation->totalResults ?
		                          GOOGY_SEARCH_RESULTS : $res->searchInformation->totalResults)
*/
		    )));
		
		if($res->searchInformation->totalResults > 0)
		{
			foreach($res->items as $i)
			{
				debuglog("i:", $i);
				
				telegram_api("sendChatAction", array(
				    'action' => 'upload_photo', 'chat_id' => $msg->message->chat->id));
				
				// Download the image so we can re-upload it
				$image_file = download_file($i->link, "Goog_");
				debuglog("download_file returned:", $image_file);
				$ret = null;
				if($image_file !== false)
				{
					$cf = curl_file_create($image_file['file'], $image_file['mimetype'], $image_file['filename']);
					
					$ret = telegram_api("sendPhoto", array(
						'chat_id' => $msg->message->chat->id,
						'reply_to_message_id' => $msg->message->message_id,
						'photo' => $cf,
						'caption' => sprintf("%s (%s)\n%s",
						                     $i->title,
						                     $i->image->contextLink,
						                     $i->snippet
						)));
					unlink($image_file['file']);
				}
				
				if($ret === false ||
				   $image_file === false)
					telegram_api("sendMessage", array(
					    'chat_id' => $msg->message->chat->id,
					    'reply_to_message_id' => $msg->message->message_id,
					    'text' => "I'm sorry, bro, the Telegram API returned an error while I was sending the requested image.\n".
					              sprintf("%s (%s) %s\n%s",
					                      $i->title,
					                      $i->image->contextLink,
					                      $i->link,
					                      $i->snippet
					    )));
			}	// foreach()
		}	//if()
	}

	if($command == "/get")
	{
		if(!$args)
		{
			telegram_api("sendMessage", array(
			    'chat_id' => $msg->message->chat->id,
			    'reply_to_message_id' => $msg->message->message_id,
			    'text' => "Hey there! I need a search term or terms in order to Google Image search."
			    ));
			return;
		}
		
		telegram_api("sendChatAction", array(
		    'action' => 'typing', 'chat_id' => $msg->message->chat->id));
		
		// Fetch the top $nresults images and pick one at random
		$nresults = 10;	// Google's limit is 10
		
		$res = google_search($args, $nresults, "image");
		
		// If we got a spelling suggestino, we need to re-Google using that.
		if(isset($res->spelling))
		{
			debuglog("Google Image provided a spelling suggestion. Re-executing search.", $res->spelling);
			$res = google_search($args, $nresults, "image");
		}
		
		debuglog("Number of results:", $res->searchInformation->totalResults);
		if($res->searchInformation->totalResults > 0)
		{
			shuffle($res->items);
			$i = $res->items[0];
			debuglog("i:", $i);
			
			telegram_api("sendChatAction", array(
			    'action' => 'upload_photo', 'chat_id' => $msg->message->chat->id));
			
			// Download the image so we can re-upload it
			$image_file = download_file($i->link, "Goog_");	// returns (bool)false on failure
			debuglog("download_file returned:", $image_file);
			$ret = null;
			if($image_file !== false)
			{
				$cf = curl_file_create($image_file['file'], $image_file['mimetype'], $image_file['filename']);
				
				// Try uploading the file. If that fails, we'll just send a message.
				$ret = telegram_api("sendPhoto", array(
					'chat_id' => $msg->message->chat->id,
					'reply_to_message_id' => $msg->message->message_id,
					'photo' => $cf,
					'caption' => sprintf("%s | %s",
					                     $i->image->contextLink,
					                     $i->link
					)));
				unlink($image_file['file']);
			}
			
			// Upload or download failed, so fallback to just a textual message
			if($ret === false ||
			   $image_file === false)
				telegram_api("sendMessage", array(
				    'chat_id' => $msg->message->chat->id,
				    'reply_to_message_id' => $msg->message->message_id,
				    'text' => sprintf("%s | %s",
				                      $i->image->contextLink,
				                      $i->link
				    )));
		} else	//if()
		{
			// XXX This seems to be tieing things up??? 
			telegram_api("sendMessage", array(
			    'chat_id' => $msg->message->chat->id,
			    'reply_to_message_id' => $msg->message->message_id,
			    'text' => sprintf("No results for '%s'",
			                      $args)
			    ));
		}
	}
}


function google_search($search, $num_results=3, $search_type=null)
{
	$url = sprintf("https://www.googleapis.com/customsearch/v1?key=%s&cx=%s&".
	               "alt=json&googlehost=google.com&filter=1&".
	               "num=%d&safe=off&fields=%s&q=%s",
	               urlencode(GOOGY_GOOGLE_API_KEY),
	               urlencode(GOOGY_GOOGLE_CSE_ID),
	               urlencode($num_results),
	               rawurlencode("items(link,snippet,title),".
	                            "queries,".
	                            "searchInformation/totalResults,".
	                            "searchInformation/formattedTotalResults,".
	                            "spelling/correctedQuery"),
	               rawurlencode($search));
	if($search_type == "image")
	{
		$url = sprintf("https://www.googleapis.com/customsearch/v1?key=%s&".
		               "cx=%s&alt=json&googlehost=google.com&filter=1&".
		               "searchType=image&num=%d&safe=off&fields=%s&q=%s",
		               urlencode(GOOGY_GOOGLE_API_KEY),
		               urlencode(GOOGY_GOOGLE_CSE_ID),
		               urlencode($num_results),
		               rawurlencode("items(snippet,title,link,mime,image/contextLink),".
		                            "queries,".
		                            "searchInformation/totalResults,".
		                            "searchInformation/formattedTotalResults,".
		                            "spelling/correctedQuery"),
		               rawurlencode($search));
	}
	
	debuglog("Request URL:", $url);
	debuglog("Request search:", $search);
	debuglog("Request num_results:", $num_results);
	
	$ch = curl_init($url);
	curl_setopt_array($ch, array(
	    CURLOPT_USERAGENT => BOT_USER_AGENT,
	    CURLOPT_AUTOREFERER => true,
	    CURLOPT_FAILONERROR => true,
	    CURLOPT_FOLLOWLOCATION => true,
	    CURLOPT_HEADER => false,
	    //CURLOPT_HEADER => true,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_PROTOCOLS => CURLPROTO_HTTP | CURLPROTO_HTTPS,
	    CURLOPT_SAFE_UPLOAD => true,
	    CURLOPT_HTTPGET => true,
	    //CURLOPT_STDERR => $log_fh
	    CURLOPT_ENCODING => "",	// deflate, gzip
	    CURLINFO_HEADER_OUT => true,
	    ));
	// curl_setopt($ch, CURLOPT_VERBOSE, true);
	
	$response = curl_exec($ch);
	debuglog("google_search response[raw]:", $response);
	debuglog("google_search curl_getinfo:", curl_getinfo($ch));
	debuglog("google_search curl_error:", curl_error($ch));
	curl_close($ch);
	
	if($response === false)
	{
		debuglog("google_search  error!");
		return false;
	}
	
	//list($headers, $body) = explode("\r\n\r\n", $response);
	//debuglog("Headers:", $headers);
	//debuglog("Body:", $body);
	//$response_json = json_decode($body);
	$response_json = json_decode($response);
	debuglog("google_search response[obj]:", $response_json);
	if(!$response_json)
	{
		codelog("google_search JSON error!");
		return false;
	}
	
#	if($response_json->ok != "true")
#		codelog("{$method} Warning, API returned an error response.", $response_json);
	
	return $response_json;
}

// Fall-through to a 204
//header("HTTP/1.1 204 No Content");
// "Bot 2:59 PM You should response with an ok, I would suggest a 200 code answer."
// Fall-through 200/ok
?>ok
